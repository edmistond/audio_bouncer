function processAudio() {
  // Create a WebSocket connection
  const ws = new WebSocket("ws://localhost:8080");

  // When the WebSocket connection is open, start streaming audio data
  ws.onopen = () => {
    console.log("Connected to server");

    // Create an AudioContext
    const audioContext = new AudioContext();
    const source = audioContext.createBufferSource();
    let audioBuffer = null;

    const constraints = {
      audio: {
        sampleRate: 44100,
      },
    };

    ws.binaryType = "arraybuffer";

    ws.addEventListener("message", (event) => {
      const data = event.data;
      console.log(data.byteLength);
      //     if (!audioBuffer) {
      //     audioContext.decodeAudioData(event.data, (buffer) => {
      //       audioBuffer = buffer;
      //       source.buffer = buffer;
      //       source.connect(audioContext.destination);
      //       source.start();
      //     });
      //   } else {
      //     const chunk = event.data;
      //     const numChannels = audioBuffer.numberOfChannels;
      //     const bytesPerSample =
      //       (constraints.audio.sampleRate * audioBuffer.duration) /
      //       audioBuffer.length;
      //     const audioData = new Float32Array(
      //       chunk,
      //       0,
      //       chunk.byteLength / bytesPerSample
      //     );
      //     const buffer = audioContext.createBuffer(
      //       numChannels,
      //       audioData.length,
      //       audioBuffer.sampleRate
      //     );
      //     for (let channel = 0; channel < numChannels; channel++) {
      //       const channelData = buffer.getChannelData(channel);
      //       const offset = channel * audioData.length;
      //       channelData.set(
      //         audioData.subarray(offset, offset + audioData.length)
      //       );
      //     }
      //     const source = audioContext.createBufferSource();
      //     source.buffer = buffer;
      //     source.connect(audioContext.destination);
      //     source.start();
      //   }
    });

    // Request permission to access the microphone
    navigator.mediaDevices
      .getUserMedia(constraints)
      .then((stream) => {
        const source = audioContext.createMediaStreamSource(stream);

        // Create a ScriptProcessorNode to process the audio data
        const scriptProcessor = audioContext.createScriptProcessor(4096, 1, 1);

        // Connect the source to the script processor
        source.connect(scriptProcessor);

        // Connect the script processor to the destination
        scriptProcessor.connect(audioContext.destination);

        // When the script processor has data, send it over the WebSocket connection
        scriptProcessor.onaudioprocess = (event) => {
          const audioData = event.inputBuffer.getChannelData(0);
          ws.send(audioData);
        };
      })
      .catch((error) => {
        console.error(error);
      });
  };

  // Handle WebSocket disconnections
  ws.onclose = () => {
    console.log("Disconnected from server");
  };
}

// Export the function for use in the browser
window.processAudio = processAudio;
