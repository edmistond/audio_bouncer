// initialize a simple node.js app
const express = require("express");
const app = express();
const server = require("http").createServer(app);
const webSocket = require("ws");
const webSocketStream = require("websocket-stream/stream");

// configure static asset serving
app.use(express.static("public"));

const wss = new webSocket.Server({ server });

wss.on("connection", (ws) => {
  console.log("Client connected");

  const stream = webSocketStream(ws);

  stream.on("data", (data) => {
    console.log("getting data of length: ", data.byteLength);
    stream.write(data);
  });

  wss.on("close", () => {
    console.log("client disconnected!");
  });
});

server.listen(8080, () => {
  console.log("listening on port 8080");
});
